﻿#include <iostream>
#include <chrono>


class Timer
{
private:
	using clock_t = std::chrono::high_resolution_clock;
	using second_t = std::chrono::duration<double, std::ratio<1> >;

	std::chrono::time_point<clock_t> m_beg;

public:
	Timer() : m_beg(clock_t::now())
	{
	}

	void reset()
	{
		m_beg = clock_t::now();
	}

	double elapsed() const
	{
		return std::chrono::duration_cast<second_t>(clock_t::now() - m_beg).count();
	}
};

struct T_list
{
	T_list* next;
	int num;

};

void Merge(int* A, int first, int last)
{
	int middle, start, final;
	int* mas = new int[last - first + 1];
	middle = (first + last) / 2; 
	start = first; 
	final = middle + 1; 
	for (int j = first; j <= last; j++) 
		if ((start <= middle) && ((final > last) || (A[start] < A[final])))
		{
			mas[j - first] = A[start];
			start++;
		}
		else
		{
			mas[j - first] = A[final];
			final++;
		}
	
	for (int j = first; j <= last; j++)
	{
		A[j] = mas[j - first];
	}
	delete[]mas;
};

void MergeSort(int* A, int first, int last)
{
	{
		if (first < last)
		{
			MergeSort(A, first, (first + last) / 2); 
			MergeSort(A, (first + last) / 2 + 1, last); 
			Merge(A, first, last); 
		}
	}
};

int main()
{
	setlocale(LC_ALL, "Rus");
	int n;
	std::cout << "Размер массива > "; 
	std::cin >> n;
	int* A = new int[10000];
	for (int i = 0; i < n; i++)
	{
		A[i] = rand();
		std::cout << A[i] << std::endl;
	}
	std::cout << "==============" << std::endl;
	Timer t;
	MergeSort(A, 0, n - 1);

	for (int i = 0; i < n; i++)
	{
		std::cout << A[i] << std::endl;
	}

	std::cout << "Time elapsed: " << t.elapsed() << '\n';

	delete[]A;
}